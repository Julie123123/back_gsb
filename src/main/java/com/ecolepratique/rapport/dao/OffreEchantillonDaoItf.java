package com.ecolepratique.rapport.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ecolepratique.rapport.entite.OffreEchantillon;

/**
 * 
 * @author Christophe
 *
 */
public interface OffreEchantillonDaoItf extends JpaRepository<OffreEchantillon, Long> {

}
